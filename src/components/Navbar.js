import React, {useState} from 'react';
import { Navbar, Nav } from 'react-bootstrap';
// import {BrowserRouter as Router, Route, Link, Switch, useRouteMatch, useParams} from 'react-router-dom';
import { LinkContainer } from 'react-router-bootstrap';
// import MuseumDisplay from './MuseumDisplay';
// import NatSciApp from './personal-projects/NatSciApp';

function NavBar(props) {
    return (
        <Navbar bg="dark" expand="lg" variant="dark">
            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
            <Navbar.Collapse id="basic-navbar-nav">


                <Nav className="mr-auto">
                    <LinkContainer to="/">
                        <Nav.Link>Cooper Hewitt API</Nav.Link>
                    </LinkContainer>

                    <LinkContainer to="/natsci">
                        <Nav.Link>NatSci Fellows App</Nav.Link>                        
                    </LinkContainer>

                    <LinkContainer to="/alshtradingco">
                        <Nav.Link>Alsh Trading Co. Website</Nav.Link>                        
                    </LinkContainer>
                
                    <LinkContainer to="/pente">
                        <Nav.Link>Pente Game</Nav.Link>                        
                    </LinkContainer>
                    

                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
}

export default NavBar;

