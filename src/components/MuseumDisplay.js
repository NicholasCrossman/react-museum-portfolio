import React, {useState, useEffect} from 'react';
import axios from 'axios';
import ItemTile from './ItemTile';
//import Navbar from './NavBar';

function MuseumDisplay(props) {

    const [page, setPage] = useState(1);
    const [itemsPerPage, setItemsPerPage] = useState(25);
    const [items, setItems] = useState([]);

    const [itemDisplay, setItemDisplay] = useState([]);


    useEffect(() => {
        loadItems();
    }, []);

    const loadItems = () => {
        axios.get(`https://api.artic.edu/api/v1/artworks?page=${page}&limit=${itemsPerPage}&fields=id,title,image_id,artist_title`)
            .then(response => {
                let returnedItems = Array.from(response.data.data);
                console.log("data: " + JSON.stringify(returnedItems));
                setItems(returnedItems);
                let renderedItems = items.map( (item) => 
        
                    <ItemTile itemName={item.title} 
                        artistName={item.artist_title} 
                        imageSrc={makeImageUrl(item.image_id)} 
                        key={item.id}/>
            
                );
                setItemDisplay(renderedItems);
            })
        return null;
    }

    const makeImageUrl = (id) => {
        return `https://www.artic.edu/iiif/2/${id}/full/843,/0/default.jpg`;
    }

    const pageForward = () => {
        setPage(page + 1);
        loadItems();
    }

    const pageBack = () => {
        if(page > 1) {
            setPage(page - 1);
            loadItems();
        }
    }

    const masonryConfig = {
        percentPosition: true
    };

    return(
        <div className="container">
            <header className="px-4 py-5 my-5 text-center">
                <h1>Art Institute of Chicago</h1>
                <h3>Displaying Items from the API</h3>
                <br/>
                <br/>
            </header>
            <div className="row" data-masonry={masonryConfig}>
                {itemDisplay}
            </div>

            <div className="row">
                <div className="btn-group mx-auto">
                    <button type="button" className="btn btn-secondary" onClick={pageBack}>
                        Previous Page <i className="bi bi-arrow-bar-left"></i>
                    </button>
                    <div className="p-3">
                        <p>Page {page}</p>
                    </div>
                    <button type="button" className="btn btn-info" onClick={pageForward}>
                        <i className="bi bi-arrow-bar-right"></i> Next Page
                    </button>
                </div>
            </div>
            
        </div>
    );
}

export default MuseumDisplay;