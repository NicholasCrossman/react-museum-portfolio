import React, {useState} from 'react';
import './ItemTile.css';

function ItemTile(props) {
    const [itemName] = useState(props.itemName);
    const [imageSrc] = useState(props.imageSrc);
    const [artistName] = useState(props.artistName);

    return(
        <div className="card p-3 m-3">
            <img className="card-img-top img-fluid" src={imageSrc} alt={itemName}/>
            <div className="card-body">
                <h4 className="card-title">{itemName}</h4>
                <p className="card-text">by {artistName}</p>
            </div>
        </div>
    )
}

export default ItemTile;