import React from "react";

function AlshTradingCo() {
    return(
        <div className="container-fluid w-70">
            <header className="px-4 py-5 my-5 text-center">
                <h1>Alsh Trading Co. Website</h1>
                <h3>Solo Project</h3>
                <h4>Fall 2020 - Present</h4>
                <br/>
                <br/>
            </header>

            
            <hr/><br/><br/>
            <div className="row d-flex justify-content-center border border-primary rounded p-5">
                <h2>Technologies Used</h2>
                <br/>
                <div className="col text-center p-2 my-auto">
                    <img className="img-fluid" width="200px" src="icons/angular.png" alt="Angular"/>
                    <h4>Angular</h4>
                </div>

                <div className="col text-center p-2 my-auto">
                    <img className="img-fluid" width="200px" src="icons/node-js.png" alt="Node.js"/>
                    <h4>Node.js</h4>
                </div>

                <div className="col text-center p-2 my-auto">
                    <img className="img-fluid" width="200px" src="icons/typescript.png" alt="TypeScript"/>
                    <h4>TypeScript</h4>
                </div>
                
            </div>
            <br/>
            <div className="container text-center">
                <h3>The Project</h3>
                <br/>
                <p className="fs-5 text-wrap">
                    The Alsh Trading Co. is a small business in New Jersey that sells 
                    Russian folk art and souvenirs to vendors and clients across the U.S. 
                    This website is written in Angular, with a Node.js + Express back-end written 
                    in TypeScript. JSON files are currently used for data persistence.
                </p>
                <br/>

                <h3>The Task</h3>
                <br/>

                <p className="fs-5 text-wrap">
                    The Alsh Trading Co. needs to manage large orders from vendors, and their 
                    current system involves a large physical catalog with product information 
                    and a very long phone call with the business owner. An online catalog would 
                    solve many problems, but it would have to replicate the organization of the 
                    physical catalog so items could be found and inventoried easily.
                </p>
                <br/>

                <h3>Use Cases</h3>
                <br/>

                <div className="bg-info w-20 text-wrap p-4">
                    <h4>Business Owner</h4>
                    <ul className="list-group d-inline-block">
                        <li className="list-group-item">I want to add new items to the catalog.</li>
                        <li className="list-group-item">I want to see a list of pending, in-progress, and completed orders.</li>
                        <li className="list-group-item">I want to see which items are ordered most frequently so I know what to stock.</li>
                    </ul><br/><br/>
                    <h4>Client</h4>
                    <ul className="list-group d-inline-block">
                        <li className="list-group-item">I want to save quantities of a certain item to my order.</li>
                        <li className="list-group-item">I want to see the status of my order.</li>
                        <li className="list-group-item">I want to see the orders I've made in the past.</li>
                    </ul>
                </div>
                
                <br/>

                <h3>The Design</h3>
                <br/>

                    <h4>Front End</h4>
                    <p className="fs-5 text-wrap">

                        I initially started this project as a way to gain experience with JavaScript, 
                        but managing the visual elements of the UI became a problem. I decided to switch to 
                        Angular, since I was already using TypeScript for the Node.js server at this point. 
                        I've implemented conditional rendering of components for Admin and standard users, and most 
                        of the functionality is complete.
                    </p>
                    <br/>

                    <h4>Back End</h4>
                    <p className="fs-5 text-wrap">

                        I began the back-end with Node.js + Express and JavaScript, but I soon became frustrated 
                        with the lack of type checking, as I was passing large objects with many fields. I took the 
                        opportunity to convert the project to TypeScript, which introduced its own challenges with 
                        the compiler. Once those were resolved, TypeScript was quite helpful.
                    </p>
                    <br/>

                    <h4>Security and Authentication</h4>
                    <p className="fs-5 text-wrap">

                        To make sure a User's orders could not be tampered with, and especially to 
                        make sure no Admin functions could be accessed maliciously, I implemented JSON Web 
                        Token authentication, as I'd worked with it on my Capstone before. The tokens are 
                        secured against tampering or copying, and they store the user's permissions to authorize 
                        each request.
                    </p>
                    <br/>
                    <div className="row align-items-center">
                        <div className="col-md">
                            <div className="card mx-auto" width="400px">
                                <img className="card-img-top" src="images/jwt.png" alt="JSON Web Token structure"/>
                                <div className="card-body">
                                    <p className="card-text">JSON Web Token structure.</p>
                                </div>
                            </div>                            
                        </div>
                        <div className="col-sm">
                            <p className="fs-5 text-wrap">The JWT consists of a series of key-value pairs 
                                holding headers and data to be transferred. These key-value pairs are hashed into the signature. 
                                Because small changes in the input cause large changes in a hash function's output, the server can 
                                easily see if the JWT's data has been altered since it was initially generated, such as a user 
                                giving themselves higher permissions than their account allows. In this case, the server needs to re-hash 
                                the headers and payload and compare it to the signature.</p>
                            </div>
                    </div>

                    
                    <br/>

            </div>

        </div>
    )
}

export default AlshTradingCo;