import React from 'react';
//import Navbar from '../NavBar';

function NatSciApp() {
    return(
        <div className="container-fluid w-70">
            <header className="px-4 py-5 my-5 text-center">
                <h1>The NatSci Fellows App</h1>
                <h3>Pacific Lutheran University Capstone</h3>
                <h4>Fall 2019 - Summer 2020</h4>
                <br/>
                <br/>
            </header>

            <div className="container-md text-center">
                <h2>Slug Icons</h2>
                <h5 className="text-muted">Drawn by group member Natalie Steinert.</h5>
                <br/>
                <p>Each slug represents a color theme for the app. Changing the app's theme also changes 
                    the 'home' icon to the corresponding slug.
                </p>
                <br/>

                <div className="card-deck d-inline-flex">
                    <div className="card p-3">
                        <div className="card-img-top">
                            <img width="300px" src="images/slugs/slug1.png" alt="Safety Slug"/>
                        </div>
                        <div className="card-body">
                            <p className="card-text">Safety Slug</p>
                        </div>
                    </div>

                    <div className="card p-3">
                        <div className="card-img-top">
                            <img width="300px" src="images/slugs/slug2.png" alt="Vampire Slug"/>
                        </div>
                        <div className="card-body">
                            <p className="card-text">Vampire Slug</p>
                        </div>
                    </div>

                    <div className="card p-3">
                        <div className="card-img-top">
                            <img width="300px" src="images/slugs/slug3.png" alt="Knight Slug"/>
                        </div>
                        <div className="card-body">
                            <p className="card-text">Knight Slug</p>
                        </div>
                    </div>

                    <div className="card p-3">
                        <div className="card-img-top">
                            <img width="300px" src="images/slugs/slug4.png" alt="Morton's Salt Slug"/>
                        </div>
                        <div className="card-body">
                            <p className="card-text">Morton's Salt Slug</p>
                        </div>
                    </div>

                    <div className="card p-3">
                        <div className="card-img-top">
                            <img width="300px" src="images/slugs/slug5.png" alt="Cowboy Slug"/>
                        </div>
                        <div className="card-body">
                            <p className="card-text">Cowboy Slug</p>
                        </div>
                    </div>
                </div>
                
            </div>
            <hr/><br/><br/>
            <div className="container text-center border border-primary rounded p-3">
                <h2>Technologies Used</h2>
                <br/>
                <img className="img-fluid" width="200px" src="icons/flutter.png" alt="Flutter"/>
                <img className="img-fluid" width="200px" src="icons/java.png" alt="Java"/>
                <img className="img-fluid" width="200px" src="icons/maria-db.png" alt="MariaDB"/>
            </div>
            <br/>
            <div className="container text-center">
                <h3>The Project</h3>
                <br/>
                <p className="fs-5 text-wrap">
                    The NatSci Fellows App was our Capstone project at Pacific Lutheran University. 
                    It consisted of a cross-platform mobile app in Flutter, with a Java server in the middle, 
                    and a MariaDB server in the back end. It was built in an Agile team of 4, from the Fall of 2019 
                    through the Summer of 2020.
                </p>
                <br/>

                <h3>The Task</h3>
                <br/>

                <p className="fs-5 text-wrap">
                    The NatSci Fellows program is part of PLU's Natural Sciences division. It encourages students 
                    to attend academic events by giving them paper booklets to track event participation. For an 
                    event of a certain type, such as a Professional Development conference or a Tutoring session, 
                    a student gets a sticker to put in that category in the booklet. Filling up part of the booklet 
                    rewards them with a prize from the teachers.

                    There are over a hundred students in the program, and the two professors in charge 
                    asked our group to replace the booklet with a phone app that would make it easier to track 
                    a student's progress, and provide new features that the physical booklets could not achieve.
                </p>
                <br/>

                <h3>Use Cases</h3>
                <br/>

                <div className="bg-info w-20 text-wrap p-4">
                    <h4>Professors</h4>
                    <ul className="list-group d-inline-block">
                        <li className="list-group-item">I want to create an event for students to attend.</li>
                        <li className="list-group-item">I want to see which students attended my event.</li>
                        <li className="list-group-item">I want to make sure students can't cheat and sign in to an event without attending it.</li>
                    </ul><br/><br/>
                    <h4>Students</h4>
                    <ul className="list-group d-inline-block">
                        <li className="list-group-item">I want to sign in to an event by scanning a QR code.</li>
                        <li className="list-group-item">I want to see the events I've attended.</li>
                        <li className="list-group-item">I want to track my progress towards the program's prizes.</li>
                        <li className="list-group-item">I want to compete with other students on a leaderboard to attend the most events.</li>
                    </ul>
                </div>
                
                <br/>

                <h3>The Design</h3>
                <br/>

                    <h4>Front End</h4>
                    <p className="fs-5 text-wrap">

                        Early on, we wanted to make a mobile app instead of a web app. We chose Google's Flutter 
                        because of its cross-platform capabilities, using Android Studio to build and test the app. 
                        I personally added a web portal for use by teachers and administrators, written in Java 
                        with Handlebars/Mustache. The web portal allows admins and professors to add and delete events, 
                        track a student's progress, and manage things more easily than in the mobile app.
                    </p>
                    <br/>

                    <h4>Back End</h4>
                    <p className="fs-5 text-wrap">

                        Our REST API endpoints are provided by a Java server with Jersey. The server stores a HashMap 
                        to keep track of active events. An active event is one that's in-progress so students can sign in. 
                        The HashMap has QR strings as keys, and maps them to the QR's respective event. A student signing into 
                        an event only has to send the QR with their REST call. Once the event is over, the entry is removed from 
                        the HashMap, and students can no longer sign in even if they save a copy of the QR code. This makes it harder 
                        for students to cheat and share the QR code with friends who didn't attend the event.

                        Our persistent data is stored in a MariaDB server. The Java server uses JDBC to connect to the SQL tables 
                        held by the MariaDB server. Temporary data that needs to be re-generated, like the QR codes, are not stored 
                        in the SQL tables.
                    </p>
                    <br/>

                    <h4>Security and Authentication</h4>
                    <p className="fs-5 text-wrap">

                        Because the app could be used to see a student's location if they just signed in to an event, the protection of 
                        user data is paramount. Both the Java server and the MariaDB server are hosted on-campus, meaning they can't be 
                        accessed at all from an off-campus IP address.

                        Since we have user accounts to track progress, we needed some kind of authentication. After some research, I implemented 
                        authorization using JSON Web Tokens, or JWTs. While the stateful method gives the user a session token, which is a key 
                        to permissions and other info held on the server, a JWT is stateless, meaning the token itself stores information 
                        on who the user is and what permissions they have, and the server only needs to verify the token is authentic and hasn't 
                        been tampered with.
                    </p>
                    <br/>
                    <div className="row align-items-center">
                        <div className="col-md">
                            <div className="card mx-auto" width="400px">
                                <img className="card-img-top" src="images/jwt.png" alt="JSON Web Token structure"/>
                                <div className="card-body">
                                    <p className="card-text">JSON Web Token structure.</p>
                                </div>
                            </div>                            
                        </div>
                        <div className="col-sm">
                            <p className="fs-5 text-wrap">The JWT consists of a series of key-value pairs 
                                holding headers and data to be transferred. These key-value pairs are hashed into the signature. 
                                Because small changes in the input cause large changes in a hash function's output, the server can 
                                easily see if the JWT's data has been altered since it was initially generated, such as a user 
                                giving themselves higher permissions than their account allows. In this case, the server needs to re-hash 
                                the headers and payload and compare it to the signature.</p>
                            </div>
                    </div>

                    
                    <br/>

            </div>

        </div>        
    );
}

export default NatSciApp;