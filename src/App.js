import './App.css';
import NavBar from './components/NavBar';
import NatSciApp from './components/personal-projects/NatSciApp';
import AlshTradingCo from './components/personal-projects/AlshTradingCo';
import {BrowserRouter as Router, Route, Link, Switch} from 'react-router-dom';
import MuseumDisplay from './components/MuseumDisplay';

function App() {
  return (
    <div className="App">
      <Router>
        <NavBar/>
        <Route exact path="/" component={MuseumDisplay}/>
        <Route path="/natsci" component={NatSciApp}/>
        <Route path="/alshtradingco" component={AlshTradingCo}/>
      </Router>
    </div>
    
  );
}
// in return
//<Route path="/" component={Home}/>

export default App;
